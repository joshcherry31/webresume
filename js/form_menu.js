// const fs = require('fs')

const form = document.querySelector("#basic_info");

var $ = (id) => {
    return document.getElementById(id);
}

async function submit_form() {   
    var firstName = $("first").value;
    var LastName = $("second").value;
    var email = $("email").value;
    var post_body = JSON.stringify({
        "FirstName": firstName,
        "LastName": LastName,
        "email": email
    });

    
    let headersList = {
        "Content-Type": "application/json"
    }
       
    try {
        const response = await fetch("http://jcwebresume.com:3000/test", {
            method: "POST",
            body: post_body,
            headers: headersList
        });
        console.log(await response.json());
    } catch (e) {
        console.error(e);
    }
}

// Take over form submission
form.addEventListener("submit", (event) => {
    event.preventDefault();
    const label = document.getElementById('submit');
    label.textContent = "Sent";
    submit_form();
});