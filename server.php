<?php
require 'vendor/autoload.php'; // Include the AWS SDK for PHP

use Aws\Sns\SnsClient;

if (isset($_POST['fullname'])) {
    $message = "Name: ". $_POST['fullname']." ";
    $topic = 'arn:aws:sns:us-east-1:600727355603:DevTopic';

    // Send SNS message
    $snsClient = new SnsClient([
        'version' => 'latest',
        'region'  => 'us-east-1' // Change to your desired region
    ]);
    
    try {
        $result = $snsClient->publish([
            'Message' => $message,
            'TopicArn' => $topic,
        ]);
        var_dump($result);
    } catch (AwsException $e) {
        // output error message if fails
        error_log($e->getMessage());
    }
    
}
?>