FROM public.ecr.aws/amazonlinux/amazonlinux:latest

# Install dependencies
RUN yum update -y && \
 yum install -y httpd

RUN rm -rf /var/etc/html/*
ADD index.php /var/www/html
ADD favicon.ico /var/www/html

RUN mkdir /var/www/html/css
ADD css /var/www/html/css

RUN mkdir /var/www/html/subpages
ADD subpages /var/www/html/subpages

RUN mkdir /var/www/html/js
ADD js /var/www/html/js

RUN mkdir /var/www/html/node_modules
ADD node_modules /var/www/html/node_modules

RUN mkdir /var/www/html/images
ADD images /var/www/html/images

EXPOSE 80

CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]