<?php
require 'vendor/autoload.php'; // Include the AWS SDK for PHP

use Aws\Sns\SnsClient;

if (isset($_POST['comments'])) {
    $message = "Message left on site - ". $_POST['comments']." ";
    // $topic = 'arn:aws:sns:us-east-1:600727355603:DevTopic';
    $topic = '';

    // Send SNS message
    $snsClient = new SnsClient([
        'version' => 'latest',
        'region'  => 'us-east-1' // Change to your desired region
    ]);
    
    try {
        $result = $snsClient->listTopics();

        foreach ($result['Topics'] as $topic) {
            $topicArn = $topic['TopicArn'];

            $tagsResult = $snsClient->listTagsForResource([
                'ResourceArn' => $topicArn,
            ]);

            foreach ($tagsResult['Tags'] as $tag) {
                $key = $tag['Key'];
                $value = $tag['Value'];
                if ($tag['Key'] === "Application" && $tag['Value'] === "WebServer") {
                    $topic = $topicArn;
                    break;
                }
            }
        }
    } catch (AwsException $e) {
        // output error message if fails
        error_log($e->getMessage());
    }

    try {
        $result = $snsClient->publish([
            'Message' => $message,
            'TopicArn' => $topic,
        ]);
    } catch (AwsException $e) {
        // output error message if fails
        error_log($e->getMessage());
    }
    
}
?>

<!-- 

<html>
    <head><title>SMTP mail</title></head>
    <body>
        <form action="<?php $_PHP_SELF ?>" method="POST">
            Name: <input type="text" name="fullname" />
            <textarea id="comments" name="comments" placeholder="If you have any comments, please leave them here"></textarea>
            <input type="submit"/>
        </form>
    </body>
</html> -->



<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="description" content="A site to explain more about my personal life, goals and provide further information on my projects. This site will accompany my resume">
        <title>Joshua Cherry Web Resume</title>
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
    </head>
    <body>
        <main>
            <h1 class="title">Joshua Cherry Web resume</h1>
            <form action="<?php $_PHP_SELF ?>" method="POST">
                <ul class="mainlink">
                    <li><p><a href="/subpages/aboutme.html" tabindex="0">About me</a></p></li>
                    <li><p><a href="https://gitlab.com/joshcherry31" target="_blank">Git repo</a></p></li>
                    <li><p><a href="/subpages/certs.html">Certifications</a></p></li>
                    <li><p><a href="/subpages/projects.html">Projects</a></p></li>
                    <!-- <li><p><a href="/subpages/form_menu.html">Test Form</a></p></li> -->
                    Comments:<br>
                    <textarea id="comments" name="comments" placeholder="If you have any comments, please leave them here"></textarea>
                    <!-- <button type="submit">Send</button> -->
                    <input type="submit"/>
                </ul>
            </form>
            <div class="contact">
                <address>Email: <a href="mailto:joshcherry31@gmail.com">joshcherry31@gmail.com</a>
                </address>
                <p>&copy; 2024 Joshua Cherry Web resume</p>
            </div>
        </main>
    </body>
</html>